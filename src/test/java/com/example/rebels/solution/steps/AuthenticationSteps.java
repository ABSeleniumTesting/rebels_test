package com.example.rebels.solution.steps;

import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.user.User;
import com.example.rebels.solution.request.user.GetLoginRequest;
import com.example.rebels.solution.request.user.GetLogoutRequest;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;

public class AuthenticationSteps {

    private User testUser;
    private GeneralResponse generalResponse;

    @When("As not logged in I send request to logout")
    public void as_not_logged_in_i_send_request_to_logout() {
        generalResponse = new GetLogoutRequest().sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("I should receive response with error code")
    public void i_should_receive_response_with_error_code() {
        assertThat(SerenityRest.lastResponse().statusCode()).isNotEqualTo(HttpStatus.SC_OK);
    }

    @When("I send GET request with proper username and password on \\/login path")
    public void i_send_get_request_with_proper_username_and_password_on_login_path() {
        testUser = Serenity.sessionVariableCalled(StepHelper.SESSION_USER());
        generalResponse = new GetLoginRequest().password(testUser.getPassword())
                .username(testUser.getUsername()).sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @When("I send GET request on \\/logout path")
    public void i_send_get_request_on_logout_path() {
        generalResponse = new GetLogoutRequest().sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }
    @Then("I should be logged out")
    public void i_should_be_logged_out() {
        assertThat(generalResponse.getMessage()).isEqualTo("ok");
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
    }

    @Then("User should be logged in")
    public void user_should_be_logged_in() {
        assertThat(generalResponse.getMessage()).containsPattern("[session: \\d]");
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
    }


    @When("I send GET request with not proper {string} of {string} on \\/login path")
    public void i_send_get_request_with_not_proper_of_on_login_path(String parameter, String value) {
        testUser = Serenity.sessionVariableCalled(StepHelper.SESSION_USER());
        if(StringUtils.equals(parameter, "username")) {
            generalResponse = new GetLoginRequest().password(testUser.getPassword())
                    .username(value).sendRequest()
                    .assertRequestSuccess()
                    .getResponseModel();
        } else {
            generalResponse = new GetLoginRequest().password(value)
                    .username(testUser.getUsername()).sendRequest()
                    .assertRequestSuccess()
                    .getResponseModel();
        }
    }
}
