package com.example.rebels.solution.steps;

import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.user.User;
import com.example.rebels.solution.request.user.DeleteUserRequest;
import com.example.rebels.solution.request.user.GetFindUserRequest;
import com.example.rebels.solution.request.user.PostCreateUserRequest;
import com.example.rebels.solution.request.user.PostCreateUsersWithArrayRequest;
import com.example.rebels.solution.request.user.PostCreateUsersWithListRequest;
import com.example.rebels.solution.request.user.PutUpdateUserRequest;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.DataTableType;
import io.cucumber.java.ParameterType;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class UserSteps {

    private User user;
    private User foundUser;
    private User update;
    private GeneralResponse generalResponse;
    private String searchedUsername;
    private List<User> userList;
    private User[] usersArray;
    @Steps
    private UserHelper userHelper;

    @DataTableType
    public User userTransformer(Map<String, String> entry) {
        return new User()
                .setId(Integer.parseInt(entry.get("id")))
                .setUsername(entry.get("username"))
                .setFirstName(entry.get("firstName"))
                .setLastName(entry.get("lastName"))
                .setEmail(entry.get("email"))
                .setPassword(entry.get("password"))
                .setPhone(entry.get("phone"))
                .setUserStatus(Integer.parseInt(entry.get("userStatus")));
    }


    @Given("As Server Client I Have a new User Model")
    public void as_server_client_i_have_a_new_user_model(List<User> users) {
        this.user = users.get(0);
    }

    @When("I send POST request on path \\/user")
    public void i_send_post_request_on_path_user() {
        generalResponse = new PostCreateUserRequest().user(user).sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("New User with proper data should be created")
    public void new_user_with_proper_data_should_be_created() {
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(generalResponse.getMessage()).isEqualTo("1");
    }

    @Given("I have User's username")
    public void i_have_user_s_username(List<String> username) {
        searchedUsername = username.get(0);
    }

    @When("Sending request of type GET to find User")
    public void sending_request_of_type_get_to_find_user() {
        foundUser = new GetFindUserRequest().username(searchedUsername)
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();

    }

    @Then("User should be found")
    public void user_should_be_found() {
        assertThat(foundUser).isNotNull();

    }

    @Given("I have non existing User with username {string}")
    public void i_have_non_existing_user_with_username(String username) {
        searchedUsername = username;
        user = new User().setUsername(username);
    }

    @Then("Have correct data")
    public void have_correct_data() {
        user = Serenity.sessionVariableCalled(StepHelper.SESSION_USER());
        userHelper.compareUsers(user, foundUser);
    }




    @When("I send DELETE request on path \\/user")
    public void i_send_delete_request_on_path_user() {
        user = Serenity.sessionVariableCalled(StepHelper.SESSION_USER());
        generalResponse = new DeleteUserRequest().username(user.getUsername())
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("User should be Deleted")
    public void user_should_be_deleted() {
        assertThat(generalResponse.getMessage()).contains(user.getUsername());
    }


    @Given("I have a {string} of users to create")
    public void i_have_a_of_users_to_create(String collection) {
        if (collection.equals("list")) {
            userList = StepHelper.acquireListOfUsers();
            userHelper.addUserListAsEvidence(userList.toString());
        } else {
            usersArray = StepHelper.acquireArrayOfUsers();
            userHelper.addUserListAsEvidence(Arrays.toString(usersArray));
        }
    }

    @When("I send POST request on path {string}")
    public void i_send_post_request_on_path(String path) {
        generalResponse = userHelper.sendingPostRequestForMultipleUsers(userList, usersArray, path);
    }

    @Then("Users should be created")
    public void users_should_be_created() {
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(generalResponse.getMessage()).isEqualTo("ok");
    }


    @After("@multiple_users")
    public void cleanUpUsers() {
        userHelper.cleanUpUsers(userList, usersArray);
    }

    @After("@update_user")
    public void cleanUpUserAfterUpdate() {

        new DeleteUserRequest().username(update.getUsername())
                .sendRequest();
    }

    @And("I have data for update")
    public void i_have_data_for_update(List<User> users) {
        update = users.get(0);
    }


    @When("Sending request of type PUT to update User")
    public void sending_request_of_type_put_to_update_user() {
        user = Serenity.sessionVariableCalled(StepHelper.SESSION_USER());
        generalResponse = new PutUpdateUserRequest().username(user.getUsername())
                .user(update)
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @When("I send invalid DELETE request on path \\/user")
    public void i_send_invalid_delete_request_on_path_user() {
        new DeleteUserRequest().username(user.getUsername())
                .sendRequest();
    }

    @When("Sending invalid request of type GET to find User")
    public void sending_invalid_request_of_type_get_to_find_user() {
        new GetFindUserRequest().username(searchedUsername)
                .sendRequest();
    }

    @Then("User should be updated")
    public void user_should_be_updated() {
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
        foundUser = new GetFindUserRequest().username(update.getUsername())
                .sendRequest()
                .getResponseModel();
        userHelper.compareUsers(foundUser, update);

    }



}
