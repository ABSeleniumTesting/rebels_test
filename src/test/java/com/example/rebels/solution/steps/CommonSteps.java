package com.example.rebels.solution.steps;

import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.model.store.order.PetOrder;
import com.example.rebels.solution.model.user.User;
import com.example.rebels.solution.request.pet.PostAddPetRequest;
import com.example.rebels.solution.request.store.PostPlaceOrderRequest;
import com.example.rebels.solution.request.user.GetLoginRequest;
import com.example.rebels.solution.request.user.PostCreateUserRequest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CommonSteps {
    private User user;
    private Pet pet;
    private List<Pet> pets;
    private PetOrder order;

    @Given("I have existing User")
    public void i_have_existing_user() {

        user = StepHelper.getSingleUser();
        new PostCreateUserRequest().user(user).sendRequest();
        Serenity.setSessionVariable(StepHelper.SESSION_USER()).to(user);

    }

    @Given("There is a Pet in store")
    public void there_is_a_pet_in_store() {
        pet = StepHelper.getTestPet();
        new PostAddPetRequest().pet(pet).sendRequest();
        Serenity.setSessionVariable(StepHelper.SESSION_PET()).to(pet);
    }


    @And("I log in")
    public void i_log_in() {
        new GetLoginRequest().password(user.getPassword())
                .username(user.getUsername()).sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("I should receive response with {int} error code")
    public void i_should_receive_response_with_error_code(Integer code) {
        assertThat(SerenityRest.lastResponse().statusCode()).isEqualTo(code);
    }

    @Given("There are a Pets in store")
    public void there_are_a_pets_in_store() {
        pets = StepHelper.getTestPets();
        Serenity.setSessionVariable(StepHelper.SESSION_PETS()).to(pets);
        pets.forEach(pet -> {
            new PostAddPetRequest().pet(pet).sendRequest();
        });

    }

    @Given("There is placed order")
    public void there_is_placed_order() {
        pet = StepHelper.getTestPet();
        order = StepHelper.getTestPetOrder();
        new PostAddPetRequest().pet(pet).sendRequest();
        new PostPlaceOrderRequest().order(order).sendRequest();
        Serenity.setSessionVariable(StepHelper.SESSION_PET()).to(pet);
        Serenity.setSessionVariable(StepHelper.SESSION_ORDER()).to(order);
    }

    @Given("There is a non existing Pet in store with id {int}")
    public void there_is_a_non_existing_pet_in_store_with_id(Integer int1) {
        Serenity.setSessionVariable(StepHelper.SESSION_NONEXISTENT_PET_ID()).to(int1);
    }

}
