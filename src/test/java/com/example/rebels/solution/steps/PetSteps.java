package com.example.rebels.solution.steps;

import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.model.pet.Status;
import com.example.rebels.solution.request.pet.DeletePetRequest;
import com.example.rebels.solution.request.pet.GetFindPetByIdRequest;
import com.example.rebels.solution.request.pet.GetFindPetsByStatusRequest;
import com.example.rebels.solution.request.pet.PostAddPetRequest;
import com.example.rebels.solution.request.pet.PostUploadPetImageRequest;
import com.example.rebels.solution.request.pet.PutUpdatePetRequest;
import com.example.rebels.solution.request.user.PutUpdateUserRequest;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PetSteps {
    private Pet testPet;
    private Pet addedOrFoundPet;
    private Pet update;
    private GeneralResponse generalResponse;
    private List<Pet> petList;
    private List<Pet> foundPets;
    private File imageToUpload;
    @Steps
    private PetHelper petHelper;

    @ParameterType(".*")
    public Status petStatus(String status) {
        status = status.replaceAll("'", "");
        return Status.fromString(status);
    }



    @When("I send Delete request on path \\/pet")
    public void i_send_delete_request_on_path_pet() {
        generalResponse = new DeletePetRequest().apiKey("special-key")
                .petId(petHelper.getPetIdToDelete())
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @When("I send Delete request with non existing pet id on path \\/pet")
    public void i_send_delete_request_with_non_existing_pet_id_on_path_pet() {
         new DeletePetRequest().apiKey("special-key")
                .petId(petHelper.getPetIdToDelete())
                .sendRequest();
    }
    @Then("The Pet should be deleted")
    public void the_pet_should_be_deleted() {
       assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
    }


    @Given("As Server Client I Have a specific {string} to add")
    public void as_server_client_i_have_a_specific_to_add(String pet) {
        testPet = StepHelper.specificTestPet(pet);
    }

    @Then("New pet should not be added to the store")
    public void new_pet_should_not_be_added_to_the_store() {
        assertThat(addedOrFoundPet).isNull();
        assertThat(SerenityRest.lastResponse().statusCode()).isNotEqualTo(HttpStatus.SC_OK);
    }


    @Given("As Server Client I Have a Pet with {petStatus} to add")
    public void as_server_client_i_have_a_pet_with_to_add(Status petStatus) {
        testPet = StepHelper.getTestPet();
        testPet.setStatus(petStatus);
    }

    @Given("As Server Client I Have a new Pet Model to add")
    public void as_server_client_i_have_a_new_pet_model_to_add() {
        testPet = StepHelper.getTestPet();
    }

    @When("I send POST request with specific Pet on path \\/pet")
    public void i_send_post_request_with_specific_pet_on_path_pet() {
        addedOrFoundPet = new PostAddPetRequest().pet(testPet)
                .sendRequest()
                .getResponseModel();
    }

    @When("I send POST request on path \\/pet")
    public void i_send_post_request_on_path_pet() {
        addedOrFoundPet = new PostAddPetRequest().pet(testPet)
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("New pet should be added to the store")
    public void new_pet_should_be_added_to_the_store() {
       assertThat(addedOrFoundPet).isNotNull();
       petHelper.comparePets(addedOrFoundPet, testPet);
    }

    @When("I send Find request on path \\/pet\\/petId")
    public void i_send_find_request_on_path_pet_pet_id() {
        testPet = Serenity.sessionVariableCalled(StepHelper.SESSION_PET());
        addedOrFoundPet = new GetFindPetByIdRequest().petId(testPet.getId())
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @When("I send Find request on path \\/pet\\/findByStatus with {petStatus}")
    public void i_send_find_request_on_path_pet_find_by_status_with(Status status) {
       foundPets = new GetFindPetsByStatusRequest().withStatuses(status)
               .sendRequest()
               .assertRequestSuccess()
               .getResponseModel();
    }

    @Then("The Pet with correct {petStatus} should be found")
    public void the_pet_with_correct_should_be_found(Status status) {
        assertThat(foundPets).isNotEmpty();
        petList = Serenity.sessionVariableCalled(StepHelper.SESSION_PETS());
        testPet = petHelper.findPetInTestDataByStatus(petList, status);
        addedOrFoundPet = petHelper.findPetInTestDataByStatusAndId(foundPets, status, testPet.getId());
        petHelper.comparePets(addedOrFoundPet, testPet);

    }

    @Then("The Pet should be found")
    public void the_pet_should_be_found() {
        petHelper.comparePets(addedOrFoundPet, testPet);
    }

    @Given("I have data for pet update")
    public void i_have_data_for_pet_update() {
        update = StepHelper.specificTestPet("test_pet_update.json");
    }

    @When("I send PUT update request on path \\/pet")
    public void i_send_put_update_request_on_path_pet() {
       update = new PutUpdatePetRequest().pet(update)
               .sendRequest()
               .assertRequestSuccess()
               .getResponseModel();
    }

    @Then("The Pet should be updated")
    public void the_pet_should_be_updated() {
        testPet = Serenity.sessionVariableCalled(StepHelper.SESSION_PET());
        assertThat(testPet.getName()).isNotEqualTo(update.getName());
        assertThat(testPet.getTags().size()).isLessThan(update.getTags().size());
    }

    @And("I have image to be uploaded")
    public void i_have_image_to_be_uploaded() {
        imageToUpload = petHelper.getPetImage();
    }

    @When("I send POST request on path \\/pet\\/petId\\/uploadImage")
    public void i_send_post_request_on_path_pet_pet_id_upload_image() {
        testPet = Serenity.sessionVariableCalled(StepHelper.SESSION_PET());
        generalResponse = new PostUploadPetImageRequest().image(imageToUpload)
                .petId(testPet.getId())
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("The image should be uploaded")
    public void the_image_should_be_uploaded() {
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(generalResponse.getMessage()).contains("File uploaded");
    }

}
