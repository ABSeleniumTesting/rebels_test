package com.example.rebels.solution.steps;

import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.user.User;
import com.example.rebels.solution.request.user.DeleteUserRequest;
import com.example.rebels.solution.request.user.PostCreateUsersWithArrayRequest;
import com.example.rebels.solution.request.user.PostCreateUsersWithListRequest;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserHelper {
    @Step("Comparing two users")
    public void compareUsers(User test, User expected) {
        assertThat(test.getId()).isEqualTo(expected.getId());
        assertThat(test.getUsername()).isEqualTo(expected.getUsername());
        assertThat(test.getFirstName()).isEqualTo(expected.getFirstName());
        assertThat(test.getLastName()).isEqualTo(expected.getLastName());
        assertThat(test.getEmail()).isEqualTo(expected.getEmail());
        assertThat(test.getPassword()).isEqualTo(expected.getPassword());
        assertThat(test.getPhone()).isEqualTo(expected.getPhone());
        assertThat(test.getUserStatus()).isEqualTo(expected.getUserStatus());
    }
    @Step("Cleaning up creating users")
    public void cleanUpUsers(List<User> userList, User[] usersArray) {
        if (userList != null && !userList.isEmpty()) {
            userList.forEach(user -> {
                new DeleteUserRequest().username(user.getUsername())
                        .sendRequest();
            });
        } else {
            Arrays.stream(usersArray).forEach(user -> {
                new DeleteUserRequest().username(user.getUsername())
                        .sendRequest();
            });
        }
    }

    public GeneralResponse sendingPostRequestForMultipleUsers(List<User> userList, User[] usersArray, String path) {
        if (path.contains("createWithList")) {
            return new PostCreateUsersWithListRequest().users(userList)
                    .sendRequest()
                    .assertRequestSuccess()
                    .getResponseModel();
        } else {
            return new PostCreateUsersWithArrayRequest().withUsers(usersArray)
                    .sendRequest()
                    .assertRequestSuccess()
                    .getResponseModel();
        }
    }

    public void addUserListAsEvidence(String evidence) {
        Serenity.recordReportData().asEvidence()
                .withTitle("user list")
                .andContents(evidence);
    }


}
