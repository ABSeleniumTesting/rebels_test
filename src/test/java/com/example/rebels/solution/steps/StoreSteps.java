package com.example.rebels.solution.steps;

import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.store.order.PetOrder;
import com.example.rebels.solution.request.store.DeleteOrderByIdRequest;
import com.example.rebels.solution.request.store.GetFindOrderByIdRequest;
import com.example.rebels.solution.request.store.GetInventoryRequest;
import com.example.rebels.solution.request.store.PostPlaceOrderRequest;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import org.apache.http.HttpStatus;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class StoreSteps {


    private PetOrder testOrder;
    private PetOrder placedOrFoundOrder;
    private GeneralResponse generalResponse;
    private Map<String, Integer> inventory;

    @When("I send DELETE request on path \\/store\\/order\\/orderId")
    public void i_send_delete_request_on_path_store_order_order_id() {
        testOrder = Serenity.sessionVariableCalled(StepHelper.SESSION_ORDER());
        generalResponse = new DeleteOrderByIdRequest().orderId(testOrder.getId())
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("The order should be Deleted")
    public void the_order_should_be_deleted() {
        assertThat(generalResponse.getCode()).isEqualTo(HttpStatus.SC_OK);
    }

    @When("I send GET request on path \\/store\\/order\\/orderId")
    public void i_send_get_request_on_path_store_order_order_id() {
        testOrder = Serenity.sessionVariableCalled(StepHelper.SESSION_ORDER());
        placedOrFoundOrder = new GetFindOrderByIdRequest().orderId(testOrder.getId())
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("The order should be found")
    public void the_order_should_be_found() {
        assertThat(testOrder.getId()).isEqualTo(placedOrFoundOrder.getId());
        assertThat(testOrder.getPetId()).isEqualTo(placedOrFoundOrder.getPetId());
        assertThat(testOrder.getQuantity()).isEqualTo(placedOrFoundOrder.getQuantity());
        assertThat(testOrder.getShipDate()).isEqualTo(placedOrFoundOrder.getShipDate());
        assertThat(testOrder.getStatus()).isEqualTo(placedOrFoundOrder.getStatus());
    }

    @And("As User I Have a new Order to place")
    public void as_user_i_have_a_new_order_to_place() {
        testOrder = StepHelper.getTestPetOrder();
    }

    @When("I send POST request on path \\/store\\/order")
    public void i_send_post_request_on_path_store_order() {
        placedOrFoundOrder = new PostPlaceOrderRequest().order(testOrder)
                .sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("New order should be placed to the store")
    public void new_order_should_be_placed_to_the_store() {
        assertThat(testOrder.getId()).isEqualTo(placedOrFoundOrder.getId());
        assertThat(testOrder.getPetId()).isEqualTo(placedOrFoundOrder.getPetId());
        assertThat(testOrder.getQuantity()).isEqualTo(placedOrFoundOrder.getQuantity());
        assertThat(testOrder.getShipDate()).isEqualTo(placedOrFoundOrder.getShipDate());
        assertThat(testOrder.getStatus()).isEqualTo(placedOrFoundOrder.getStatus());

    }


    @When("I send GET request on path \\/store\\/inventory")
    public void i_send_get_request_on_path_store_inventory() {
        inventory = new GetInventoryRequest().sendRequest()
                .assertRequestSuccess()
                .getResponseModel();
    }

    @Then("User inventory should be returned")
    public void user_inventory_should_be_returned() {
        assertThat(inventory).isNotNull();
    }
}
