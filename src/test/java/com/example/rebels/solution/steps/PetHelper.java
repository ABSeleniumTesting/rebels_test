package com.example.rebels.solution.steps;

import com.example.rebels.solution.config.ResourceReader;
import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.model.pet.Status;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PetHelper {

    @Step("Finding pet by status in test data")
    public Pet findPetInTestDataByStatus(List<Pet> petList, Status status) {
        return petList.stream().filter(pet -> pet.getStatus().equals(status))
                .findFirst()
                .orElseThrow();
    }

    @Step("Finding pet by id and status in test data")
    public Pet findPetInTestDataByStatusAndId(List<Pet> petList, Status status, int petId) {
        return petList.stream().filter(pet -> pet.getStatus().equals(status))
                .findFirst()
                .orElseThrow();
    }

    public int getPetIdToDelete() {
        Pet pet = Serenity.sessionVariableCalled(StepHelper.SESSION_PET());
        if (pet != null) {
            return pet.getId();
        } else {
            return Serenity.sessionVariableCalled(StepHelper.SESSION_NONEXISTENT_PET_ID());
        }
    }

    @Step("Compare found/created pet with test data")
    public void comparePets(Pet createdOrFoundPet, Pet testPet) {
        assertThat(createdOrFoundPet.getId()).isEqualTo(testPet.getId());
        assertThat(createdOrFoundPet.getCategory().getName()).isEqualTo(testPet.getCategory().getName());
        assertThat(createdOrFoundPet.getCategory().getId()).isEqualTo(testPet.getCategory().getId());
        assertThat(createdOrFoundPet.getPhotoUrls()).isEqualTo(testPet.getPhotoUrls());
        assertThat(createdOrFoundPet.getTags()).isEqualTo(testPet.getTags());
        assertThat(createdOrFoundPet.getTags()).containsExactlyElementsOf(testPet.getTags());
        assertThat(createdOrFoundPet.getStatus()).isEqualTo(testPet.getStatus());
    }

    @Step("Acquire pet image")
    public File getPetImage() {
        return ResourceReader.loadFileFromTestResources(PetHelper.class, StepHelper.TEST_PETS_FOLDER() + "/wolf.jpg");
    }
}
