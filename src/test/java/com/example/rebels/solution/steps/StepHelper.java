package com.example.rebels.solution.steps;

import com.example.rebels.solution.config.ResourceReader;
import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.model.store.order.PetOrder;
import com.example.rebels.solution.model.user.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class StepHelper {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String TEST_DATA_FOLDER = "/data";
    @Getter
    @Accessors(fluent = true)
    private final String TEST_PETS_FOLDER = TEST_DATA_FOLDER + "/pet";
    private final String TEST_ORDER_FOLDER = TEST_DATA_FOLDER + "/store";
    private final String PATH_TO_USERS_LIST = TEST_DATA_FOLDER + "/user/multiple_users_list.json";
    private final String PATH_TO_USER = TEST_DATA_FOLDER + "/user/single_user.json";
    private final String PATH_TO_PET = TEST_PETS_FOLDER + "/test_pet.json";
    private final String PATH_TO_PETS = TEST_PETS_FOLDER + "/test_pets.json";
    private final String PATH_TO_ORDER = TEST_ORDER_FOLDER + "/order.json";
    @Getter
    @Accessors(fluent = true)
    private final String SESSION_USER = "existing user";
    @Getter
    @Accessors(fluent = true)
    private final String SESSION_ORDER = "placed order";
    @Getter
    @Accessors(fluent = true)
    private final String SESSION_PET = "test pet";
    @Getter
    @Accessors(fluent = true)
    private final String SESSION_PETS = "test pet list";
    @Getter
    @Accessors(fluent = true)
    private final String SESSION_NONEXISTENT_PET_ID = "nonexistent pet id";
    @Getter
    @Accessors(fluent = true)
    private final String CURRENT_GENERAL_RESPONSE = "current general response";

    @SneakyThrows
    public List<User> acquireListOfUsers() {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, PATH_TO_USERS_LIST),
                new TypeReference<>() {
                }
        );
    }

    @SneakyThrows
    public User[] acquireArrayOfUsers() {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, PATH_TO_USERS_LIST),
                new TypeReference<>() {
                }
        );
    }

    @SneakyThrows
    public User getSingleUser() {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, PATH_TO_USER),
                User.class
        );
    }

    @SneakyThrows
    public Pet getTestPet() {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, PATH_TO_PET),
                Pet.class
        );
    }

    @SneakyThrows
    public Pet specificTestPet(String filename) {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, TEST_PETS_FOLDER + "/" + filename),
                Pet.class
        );
    }


    @SneakyThrows
    public List<Pet> getTestPets() {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, PATH_TO_PETS),
                new TypeReference<>(){}
        );
    }

    @SneakyThrows
    public PetOrder getTestPetOrder() {
        return objectMapper.readValue(
                ResourceReader.loadFileFromTestResources(StepHelper.class, PATH_TO_ORDER),
                PetOrder.class
        );
    }
}
