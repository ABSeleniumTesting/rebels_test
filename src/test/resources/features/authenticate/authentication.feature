Feature: Authentication
  Verifying login and logout functionality

  Background:
    Given I have existing User

  Scenario: Successful login
    When I send GET request with proper username and password on /login path
    Then User should be logged in

  Scenario: Successful logout
    And I log in
    When I send GET request on /logout path
    Then I should be logged out

  Scenario Outline: Unsuccessful login
    When I send GET request with not proper '<parameterName>' of '<value>' on /login path
    Then I should receive response with 400 error code

    Examples:
      | parameterName | value       |
      | username      | badusername |
      | password      | badpassword |

  Scenario: Logout of not logged in user
    When As not logged in I send request to logout
    Then I should receive response with error code