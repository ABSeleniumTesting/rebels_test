Feature: Delete Pet from the store
  Verifying deletion of a pet from the store via API

  Scenario: Delete a Pet with DELETE
    Given There is a Pet in store
    When I send Delete request on path /pet
    Then The Pet should be deleted


  Scenario: Delete a non existing Pet with DELETE
    Given There is a non existing Pet in store with id 10
    When I send Delete request with non existing pet id on path /pet
    Then I should receive response with 404 error code