Feature: Add Pet to the store
  Verifying creation/addition of a pet to the store via API

  Scenario: Add a new Pet with POST
    Given As Server Client I Have a new Pet Model to add
    When I send POST request on path /pet
    Then New pet should be added to the store

  Scenario Outline: Adding Pet with various statuses
    Given As Server Client I Have a Pet with '<petStatus>' to add
    When I send POST request on path /pet
    Then New pet should be added to the store

    Examples:
      | petStatus |
      | pending   |
      | sold      |
      | available |

  Scenario Outline: Adding Pet without specific attribute
    Given As Server Client I Have a specific '<Pet>' to add
    When I send POST request with specific Pet on path /pet
    Then New pet should not be added to the store

    Examples:
      | Pet                              |
      | test_pet_without_categories.json |
      | test_pet_without_tags.json       |
