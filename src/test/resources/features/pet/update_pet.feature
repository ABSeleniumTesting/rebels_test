Feature: Update Pet from the store
  Verifying if pet in the store can be updated via API

  Scenario: Update a Pet with PUT
    Given There is a Pet in store
    And I have data for pet update
    When I send PUT update request on path /pet
    Then The Pet should be updated