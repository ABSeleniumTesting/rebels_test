Feature: Upload Pet image to the store
  Verifying if pet image  can be uploaded via API

  Scenario: Upload Pet image with POST
    Given There is a Pet in store
    And I have image to be uploaded
    When I send POST request on path /pet/petId/uploadImage
    Then The image should be uploaded