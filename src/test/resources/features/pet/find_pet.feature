Feature: Find Pet from the store
  Verifying if pet can be found in the store via API

  Scenario: Find a Pet by ID with GET
    Given There is a Pet in store
    When I send Find request on path /pet/petId
    Then The Pet should be found


  Scenario Outline: : Find a Pet by Status with GET
    Given There are a Pets in store
    When I send Find request on path /pet/findByStatus with '<petStatus>'
    Then The Pet with correct '<petStatus>' should be found

    Examples:
      | petStatus |
      | pending   |
      | sold      |
      | available |