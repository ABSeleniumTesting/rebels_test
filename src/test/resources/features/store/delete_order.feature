Feature: Delete Order
  Verifying if user can delete order store via API

  Scenario: Place delete Order with POST
    Given There is placed order
    When I send DELETE request on path /store/order/orderId
    Then The order should be Deleted