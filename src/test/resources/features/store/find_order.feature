Feature: Find Order
  Verifying if user can find order store via API

  Scenario: Find Order with GET
    Given There is placed order
    When I send GET request on path /store/order/orderId
    Then The order should be found