Feature: Get Inventory
  Verifying if user can get his inventory store via API

  Scenario: Place new Order with POST
    Given There is a Pet in store
    And  There is placed order
    When I send GET request on path /store/inventory
    Then User inventory should be returned