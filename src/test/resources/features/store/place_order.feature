Feature: Place Order
  Verifying if user can place order store via API

  Scenario: Place new Order with POST
    Given There is a Pet in store
    And  As User I Have a new Order to place
    When I send POST request on path /store/order
    Then New order should be placed to the store