Feature: Find User
  Verifying if we can find user via API

  Scenario: Find User by username with GET
    Given I have existing User
    And I have User's username
      | Test1 |
    When Sending request of type GET to find User
    Then User should be found
    And Have correct data


  Scenario: Find non existing user
    Given I have non existing User with username 'T'
    When Sending invalid request of type GET to find User
    Then I should receive response with 404 error code