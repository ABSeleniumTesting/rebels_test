Feature: Update User
  Verifying update of user via API

  Background:
    Given I have existing User

  @update_user
  Scenario Outline: Update User by <attribute> with PUT

    And I have data for update
      | id   | username   | firstName   | lastName   | email   | password   | phone   | userStatus   |
      | <id> | <username> | <firstName> | <lastName> | <email> | <password> | <phone> | <userStatus> |
    When Sending request of type PUT to update User
    Then User should be updated

    @update_user
    Examples:
      | attribute  | id | username | firstName | lastName | email           | password | phone     | userStatus |
      | id         | 2  | Test1    | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
      | username   | 1  | Test5    | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
      | firstName  | 1  | Test1    | Test5     | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
      | lastName   | 1  | Test1    | Test1     | Test5    | test1@gmail.com | test1    | 500400900 | 1          |
      | email      | 1  | Test1    | Test1     | Test1    | test5@gmail.com | test1    | 500400900 | 1          |
      | password   | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test5    | 500400900 | 1          |
      | phone      | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test1    | 500400901 | 1          |
      | userStatus | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 5          |


  Scenario Outline: Update User without given <attribute>
    And I have data for update
      | id   | username   | firstName   | lastName   | email   | password   | phone   | userStatus   |
      | <id> | <username> | <firstName> | <lastName> | <email> | <password> | <phone> | <userStatus> |
    When Sending request of type PUT to update User
    Then I should receive response with 400 error code

    Examples:
      | attribute  | id | username | firstName | lastName | email           | password | phone     | userStatus |
      | id         | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
      | username   | 1  |          | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
      | firstName  | 1  | Test1    |           | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
      | lastName   | 1  | Test1    | Test1     |          | test1@gmail.com | test1    | 500400900 | 1          |
      | email      | 1  | Test1    | Test1     | Test1    |                 | test1    | 500400900 | 1          |
      | password   | 1  | Test1    | Test1     | Test1    | test1@gmail.com |          | 500400900 | 1          |
      | phone      | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test1    |           | 1          |
      | userStatus | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 0          |
