Feature: Delete User
  Verifying deletion of user via API

  Scenario: Delete User with DELETE
    Given I have existing User
    When I send DELETE request on path /user
    Then User should be Deleted

  Scenario: Delete non existing user
    Given I have non existing User with username 'T'
    When I send invalid DELETE request on path /user
    Then I should receive response with 404 error code