Feature: Create User
  Verifying creation of user via API

  Scenario: Create new User with POST
    Given As Server Client I Have a new User Model
      | id | username | firstName | lastName | email           | password | phone     | userStatus |
      | 1  | Test1    | Test1     | Test1    | test1@gmail.com | test1    | 500400900 | 1          |
    When I send POST request on path /user
    Then New User with proper data should be created


