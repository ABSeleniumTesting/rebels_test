Feature: Create Users
  Verifying creation of users via API

  @multiple_users
  Scenario Outline: : Creating multiple Users by <collection> with POST
    Given I have a '<collection>' of users to create
    When I send POST request on path '<multipleUsersPath>'
    Then Users should be created

    @multiple_users
    Examples:
      | collection | multipleUsersPath     |
      | list       | /user/createWithList  |
      | array      | /user/createWithArray |
