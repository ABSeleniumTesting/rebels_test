package com.example.rebels.solution.model.pet;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Status {
    AVAILABLE("available"),
    PENDING("pending"),
    SOLD("sold");

    private String key;

    Status (String key) {
        this.key = key;
    }

    @JsonValue
    public String getKey() {
        return name().toLowerCase();
    }
    @JsonCreator
    public static Status fromString(String key) {
       return key == null ? null : Status.valueOf(key.toUpperCase());
    }

}
