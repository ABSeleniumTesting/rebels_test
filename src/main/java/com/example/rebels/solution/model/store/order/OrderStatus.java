package com.example.rebels.solution.model.store.order;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum OrderStatus {
    PLACED("placed"),
    APPROVED("approved"),
    DELIVERED("delivered");

    private String key;

    OrderStatus (String key) {
        this.key = key;
    }

    @JsonValue
    public String getKey() {
        return name().toLowerCase();
    }
    @JsonCreator
    public static OrderStatus fromString(String key) {
        return key == null ? null : OrderStatus.valueOf(key.toUpperCase());
    }
}
