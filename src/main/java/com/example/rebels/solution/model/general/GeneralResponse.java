package com.example.rebels.solution.model.general;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GeneralResponse {
    private int code;
    private String type;
    private String message;
}
