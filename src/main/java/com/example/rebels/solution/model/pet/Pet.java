package com.example.rebels.solution.model.pet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Pet {
    private int id;
    private PetCategory category;
    private String name;
    private List<String> photoUrls;
    private List<PetCategory> tags;
    private Status status;
}
