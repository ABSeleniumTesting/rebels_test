package com.example.rebels.solution.model.pet;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PetCategory {
    private int id;
    private String name;
}
