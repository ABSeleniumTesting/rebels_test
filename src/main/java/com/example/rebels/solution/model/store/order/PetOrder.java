package com.example.rebels.solution.model.store.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PetOrder {
    private int id;
    private int petId;
    private int quantity;
    private Date shipDate;
    private OrderStatus status;
    private boolean complete;
}
