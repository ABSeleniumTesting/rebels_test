package com.example.rebels.solution.request.pet;

import com.example.rebels.solution.endpoint.PetEndpoint;
import com.example.rebels.solution.model.pet.Pet;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PutUpdatePetRequest extends PetEndpoint<PutUpdatePetRequest, Pet> {
    @Override
    protected Type getModel() {
        return Pet.class;
    }

    @Override
    public PutUpdatePetRequest sendRequest() {
        response = getServiceApi().updatePet(pet);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
