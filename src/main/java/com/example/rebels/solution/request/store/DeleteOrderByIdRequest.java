package com.example.rebels.solution.request.store;

import com.example.rebels.solution.endpoint.StoreEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class DeleteOrderByIdRequest extends StoreEndpoint<DeleteOrderByIdRequest, GeneralResponse> {
    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public DeleteOrderByIdRequest sendRequest() {
        response = getServiceApi().deleteOrderById(orderId);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
