package com.example.rebels.solution.request.user;

import com.example.rebels.solution.endpoint.UserEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.user.User;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostCreateUsersWithArrayRequest extends UserEndpoint<PostCreateUsersWithArrayRequest, GeneralResponse> {
    private User[] arrayOfUser;

    public PostCreateUsersWithArrayRequest withUsers(User... users) {
        this.arrayOfUser = users;
        return this;
    }

    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public PostCreateUsersWithArrayRequest sendRequest() {
        response = getServiceApi().createUsersWithArray(arrayOfUser);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
