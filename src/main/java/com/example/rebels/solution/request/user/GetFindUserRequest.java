package com.example.rebels.solution.request.user;

import com.example.rebels.solution.endpoint.UserEndpoint;
import com.example.rebels.solution.model.user.User;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class GetFindUserRequest extends UserEndpoint<GetFindUserRequest, User> {
    @Override
    protected Type getModel() {
        return User.class;
    }

    @Override
    public GetFindUserRequest sendRequest() {
        response = getServiceApi().getUser(username);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
