package com.example.rebels.solution.request.store;

import com.example.rebels.solution.endpoint.StoreEndpoint;
import com.example.rebels.solution.model.store.order.PetOrder;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostPlaceOrderRequest extends StoreEndpoint<PostPlaceOrderRequest, PetOrder> {
    @Setter
    @Accessors(fluent = true)
    private PetOrder order;

    @Override
    protected Type getModel() {
        return PetOrder.class;
    }

    @Override
    public PostPlaceOrderRequest sendRequest() {
        response = getServiceApi().placeOrder(order);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
