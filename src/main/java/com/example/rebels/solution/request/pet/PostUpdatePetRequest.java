package com.example.rebels.solution.request.pet;

import com.example.rebels.solution.endpoint.PetEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import com.example.rebels.solution.model.pet.Status;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostUpdatePetRequest extends PetEndpoint<PostUpdatePetRequest, GeneralResponse> {
    @Setter
    @Accessors(fluent = true)
    private Status status;
    @Setter
    @Accessors(fluent = true)
    private String name;

    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public PostUpdatePetRequest sendRequest() {
        if(status != null && name != null) {
            response = getServiceApi().updatePet(petId, name, status);
        } else if (status != null) {
            response = getServiceApi().updatePet(petId, status);
        } else if (name != null) {
            response = getServiceApi().updatePet(petId, name);
        } else {
            response = getServiceApi().updatePet(petId);
        }
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
