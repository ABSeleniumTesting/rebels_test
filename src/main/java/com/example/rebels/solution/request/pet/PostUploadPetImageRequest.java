package com.example.rebels.solution.request.pet;

import com.example.rebels.solution.endpoint.PetEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import java.io.File;
import java.lang.reflect.Type;

public class PostUploadPetImageRequest extends PetEndpoint<PostUploadPetImageRequest, GeneralResponse> {

    @Setter
    @Accessors(fluent = true)
    private File image;
    @Setter
    @Accessors(fluent = true)
    private String additionalMetadata;

    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public PostUploadPetImageRequest sendRequest() {
        if(additionalMetadata != null) {
            response = getServiceApi().uploadPetImage(petId, image, additionalMetadata);
        } else {
            response = getServiceApi().uploadPetImage(petId, image);
        }
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
