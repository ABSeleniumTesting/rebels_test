package com.example.rebels.solution.request.store;

import com.example.rebels.solution.endpoint.StoreEndpoint;
import com.example.rebels.solution.model.store.order.PetOrder;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class GetFindOrderByIdRequest extends StoreEndpoint<GetFindOrderByIdRequest, PetOrder> {
    @Override
    protected Type getModel() {
        return PetOrder.class;
    }

    @Override
    public GetFindOrderByIdRequest sendRequest() {
        response = getServiceApi().findOrderById(orderId);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
