package com.example.rebels.solution.request.user;

import com.example.rebels.solution.endpoint.UserEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class GetLogoutRequest extends UserEndpoint<GetLogoutRequest, GeneralResponse> {
    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public GetLogoutRequest sendRequest() {
        response = getServiceApi().logout();
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
