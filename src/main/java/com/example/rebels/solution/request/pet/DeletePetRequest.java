package com.example.rebels.solution.request.pet;

import com.example.rebels.solution.endpoint.PetEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class DeletePetRequest extends PetEndpoint<DeletePetRequest, GeneralResponse> {
    @Setter
    @Accessors(fluent = true)
    private String apiKey;

    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public DeletePetRequest sendRequest() {
        response = getServiceApi().deletePet(petId, apiKey);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
