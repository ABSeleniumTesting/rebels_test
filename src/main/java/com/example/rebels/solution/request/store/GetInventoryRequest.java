package com.example.rebels.solution.request.store;

import com.example.rebels.solution.endpoint.StoreEndpoint;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.Map;

public class GetInventoryRequest extends StoreEndpoint<GetInventoryRequest, Map<String, Integer>> {
    @Override
    protected Type getModel() {
        return TypeFactory.defaultInstance().constructMapLikeType(Map.class, String.class, Integer.class);
    }

    @Override
    public GetInventoryRequest sendRequest() {
        response = getServiceApi().getInventory();
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
