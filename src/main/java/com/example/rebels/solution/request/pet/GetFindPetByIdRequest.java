package com.example.rebels.solution.request.pet;

import com.example.rebels.solution.endpoint.PetEndpoint;
import com.example.rebels.solution.model.pet.Pet;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class GetFindPetByIdRequest extends PetEndpoint<GetFindPetByIdRequest, Pet> {
    @Override
    protected Type getModel() {
        return Pet.class;
    }

    @Override
    public GetFindPetByIdRequest sendRequest() {
        response = getServiceApi().findPetById(petId);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
