package com.example.rebels.solution.request.user;

import com.example.rebels.solution.endpoint.UserEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class PostCreateUserRequest extends UserEndpoint<PostCreateUserRequest, GeneralResponse> {
    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public PostCreateUserRequest sendRequest() {
        response = getServiceApi().createUser(user);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
