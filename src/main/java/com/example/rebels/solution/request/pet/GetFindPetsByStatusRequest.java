package com.example.rebels.solution.request.pet;

import com.example.rebels.solution.endpoint.PetEndpoint;
import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.model.pet.Status;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;

public class GetFindPetsByStatusRequest extends PetEndpoint<GetFindPetsByStatusRequest, List<Pet>> {

    private String[] statuses;

    public GetFindPetsByStatusRequest withStatuses(Status... statuses) {
        this.statuses = Arrays.stream(statuses).map(Status::getKey).toArray(String[]::new);
        return this;
    }

    @Override
    protected Type getModel() {
        return TypeFactory.defaultInstance().constructCollectionLikeType(List.class, Pet.class);
    }

    @Override
    public GetFindPetsByStatusRequest sendRequest() {
        response = getServiceApi().findPetsByStatus(statuses);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
