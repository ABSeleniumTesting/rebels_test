package com.example.rebels.solution.request.user;

import com.example.rebels.solution.endpoint.UserEndpoint;
import com.example.rebels.solution.model.general.GeneralResponse;
import io.restassured.response.Response;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.http.HttpStatus;

import java.lang.reflect.Type;

public class GetLoginRequest extends UserEndpoint<GetLoginRequest, GeneralResponse> {

    @Setter
    @Accessors(fluent = true)
    private String password;

    @Override
    protected Type getModel() {
        return GeneralResponse.class;
    }

    @Override
    public GetLoginRequest sendRequest() {
        response = getServiceApi().login(username, password);
        return this;
    }

    @Override
    protected int getSuccessStatusCode() {
        return HttpStatus.SC_OK;
    }
}
