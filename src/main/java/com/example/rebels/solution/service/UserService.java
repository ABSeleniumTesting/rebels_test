package com.example.rebels.solution.service;

import com.example.rebels.solution.model.user.User;
import io.restassured.response.Response;

import java.util.List;

/**
 * The representation User service.
 */
public class UserService extends BaseService {

    private final String USERNAME_PARAM_LABEL = "username";
    private final String USERNAME_IN_PATH = String.format("/{%s}", USERNAME_PARAM_LABEL);


    /**
     * Creates users from given array.
     *
     * @param users the users
     * @return the response
     */
    public Response createUsersWithArray(User... users) {
        return getBaseSpec().body(users)
                .post("/createWithArray");
    }

    /**
     * Creates users from given list.
     *
     * @param users the users
     * @return the response
     */
    public Response createUsersWithList(List<User> users) {
        return getBaseSpec().body(users)
                .post("/createWithList");
    }

    /**
     * Gets user by his username.
     *
     * @param username the username
     * @return the user
     */
    public Response getUser(String username) {
        return getBaseSpec().pathParam(USERNAME_PARAM_LABEL, username)
                .get(USERNAME_IN_PATH);
    }

    /**
     * Updates user.
     *
     * @param user     the user
     * @param username the username
     * @return the response
     */
    public Response updateUser(User user, String username) {
        return getBaseSpec().pathParam(USERNAME_PARAM_LABEL, username)
                .body(user)
                .put(USERNAME_IN_PATH);
    }

    /**
     * Deletes user.
     *
     * @param username the username
     * @return the response
     */
    public Response deleteUser(String username) {
        return getBaseSpec().pathParam(USERNAME_PARAM_LABEL, username)
                .delete(USERNAME_IN_PATH);
    }

    /**
     * Logout from store.
     *
     * @return the response
     */
    public Response logout() {
        return getBaseSpec().get("/logout");
    }

    /**
     * Login to the store.
     *
     * @param username the username
     * @param password the password
     * @return the response
     */
    public Response login(String username, String password) {
        return getBaseSpec().queryParam(USERNAME_PARAM_LABEL, username)
                .queryParam("password", password)
                .get("/login");
    }

    /**
     * Create user.
     *
     * @param user the user
     * @return the response
     */
    public Response createUser(User user) {
        return getBaseSpec().body(user)
                .post();
    }

    @Override
    protected String serviceBasePath() {
        return "/user";
    }
}
