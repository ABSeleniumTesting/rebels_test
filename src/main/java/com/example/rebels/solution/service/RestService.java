package com.example.rebels.solution.service;

public interface RestService<T> {

    T getServiceApi();
}
