package com.example.rebels.solution.service;

import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.model.pet.Status;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.File;

/**
 * The representation Pet service.
 */
public class PetService extends BaseService {
    private final String PET_ID_PATH_PARAM = "petId";
    private final String PET_ID_IN_PATH = String.format("/{%s}", PET_ID_PATH_PARAM);

    @Override
    protected String serviceBasePath() {
        return "/pet";
    }


    /**
     * Adds pet to the store.
     *
     * @param pet the pet
     * @return the response
     */
    public Response addPet(Pet pet) {
        return getBaseSpec().body(pet)
                .post();
    }

    /**
     * Updates pet that exist in the store.
     *
     * @param pet the pet
     * @return the response
     */
    public Response updatePet(Pet pet) {
        return getBaseSpec().body(pet)
                .put();
    }

    /**
     * Finds pets in the store by status.
     *
     * @param statuses the statuses
     * @return the response
     */
    public Response findPetsByStatus(String...statuses) {
        return getBaseSpec().queryParam("status", statuses)
                .get("/findByStatus");
    }

    /**
     * Find pet in the store by id.
     *
     * @param petId the pet id
     * @return the response
     */
    public Response findPetById(int petId) {
        return getBaseSpec().pathParam(PET_ID_PATH_PARAM, petId)
                .get(PET_ID_IN_PATH);
    }

    /**
     * Updates pet in the store.
     *
     * @param petId the pet id
     * @return the response
     */
    public Response updatePet(int petId) {
        return getBaseSpec().pathParam(PET_ID_PATH_PARAM, petId)
                .post(PET_ID_IN_PATH);
    }

    /**
     * Updates pet in the store.
     *
     * @param petId the pet id
     * @param name  the name
     * @return the response
     */
    public Response updatePet(int petId, String name) {
        return getBaseSpec().pathParam(PET_ID_PATH_PARAM, petId)
                .formParam("name", name)
                .post(PET_ID_IN_PATH);
    }

    /**
     * Updates pet in the store.
     *
     * @param petId  the pet id
     * @param status the status
     * @return the response
     */
    public Response updatePet(int petId, Status status) {
        return getBaseSpec().pathParam(PET_ID_PATH_PARAM, petId)
                .formParam("status", status)
                .post(PET_ID_IN_PATH);
    }

    /**
     * Updates pet in the store.
     *
     * @param petId  the pet id
     * @param name   the name
     * @param status the status
     * @return the response
     */
    public Response updatePet(int petId, String name, Status status) {
        return getBaseSpec().pathParam(PET_ID_PATH_PARAM, petId)
                .formParam("name", name)
                .formParam("status", status)
                .post(PET_ID_IN_PATH);
    }

    /**
     * Upload pets image.
     *
     * @param petId the pet id
     * @param image the image
     * @return the response
     */
    public Response uploadPetImage(int petId, File image) {
        return getBaseSpec().contentType("multipart/form-data")
                .pathParam(PET_ID_PATH_PARAM, petId)
                .multiPart("file", image)
                .post(PET_ID_IN_PATH + "/uploadImage");
    }

    /**
     * Upload pets image.
     *
     * @param petId              the pet id
     * @param image              the image
     * @param additionalMetadata the additional metadata
     * @return the response
     */
    public Response uploadPetImage(int petId, File image, String additionalMetadata) {
        return getBaseSpec().contentType("multipart/form-data")
                .pathParam(PET_ID_PATH_PARAM, petId)
                .multiPart("file", image)
                .formParam("additionalMetadata", additionalMetadata)
                .post(PET_ID_IN_PATH + "/uploadImage");
    }

    /**
     * Delete pet form the store.
     *
     * @param petId  the pet id
     * @param apiKey the api key
     * @return the response
     */
    public Response deletePet(int petId, String apiKey) {
        return getBaseSpec().header("api_key", apiKey)
                .pathParam(PET_ID_PATH_PARAM, petId)
                .delete(PET_ID_IN_PATH);
    }
}
