package com.example.rebels.solution.service;

import com.example.rebels.solution.model.store.order.PetOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.restassured.response.Response;
import lombok.SneakyThrows;

/**
 * The representation Store service.
 */
public class StoreService extends BaseService {

    private final String ORDER_SUB_RESOURCE = "/order";
    private final String ORDER_ID_PATH_PARAM = "orderId";
    private final String ORDER_ID_IN_PATH = String.format("/{%s}", ORDER_ID_PATH_PARAM);

    @Override
    protected String serviceBasePath() {
        return "/store";
    }

    /**
     * Place order in the store.
     *
     * @param petOrder the pet order
     * @return the response
     */
    @SneakyThrows
    public Response placeOrder(PetOrder petOrder) {
        ObjectMapper objectMapper = new ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        return getBaseSpec().body(objectMapper.writeValueAsString(petOrder))
                .post(ORDER_SUB_RESOURCE);
    }

    /**
     * Find order by id.
     *
     * @param orderId the order id
     * @return the response
     */
    public Response findOrderById(int orderId) {
        return getBaseSpec().pathParam(ORDER_ID_PATH_PARAM, orderId)
                .get(ORDER_SUB_RESOURCE + ORDER_ID_IN_PATH);
    }

    /**
     * Delete order from store by id.
     *
     * @param orderId the order id
     * @return the response
     */
    public Response deleteOrderById(int orderId) {
        return getBaseSpec().pathParam(ORDER_ID_PATH_PARAM, orderId)
                .delete(ORDER_SUB_RESOURCE + ORDER_ID_IN_PATH);
    }

    /**
     * Gets inventory.
     *
     * @return the inventory
     */
    public Response getInventory() {
        return getBaseSpec().get("/inventory");
    }
}
