package com.example.rebels.solution.service;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public abstract class BaseService {
    private final String PET_STORE_API = "https://petstore.swagger.io/v2/";

    protected RequestSpecification getBaseSpec() {
        return SerenityRest.with().baseUri(PET_STORE_API).basePath(serviceBasePath())
                .response().log().all()
                .request().contentType(ContentType.JSON);
    }

    protected abstract String serviceBasePath();


}
