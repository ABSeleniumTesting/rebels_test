package com.example.rebels.solution.endpoint;

import com.example.rebels.solution.model.user.User;
import com.example.rebels.solution.service.RestService;
import com.example.rebels.solution.service.UserService;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

public abstract class UserEndpoint<E, M> extends BaseEndpoint<E, M> implements RestService<UserService> {
    @Setter
    @Accessors(fluent = true)
    protected User user;
    @Setter
    @Accessors(fluent = true)
    protected String username;
    @Setter
    @Accessors(fluent = true)
    protected List<User> users;


    @Override
    public UserService getServiceApi() {
        return new UserService();
    }
}
