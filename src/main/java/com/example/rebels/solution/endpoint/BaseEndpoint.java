package com.example.rebels.solution.endpoint;

import com.thoughtworks.xstream.io.json.AbstractJsonWriter;
import io.restassured.response.Response;

import java.lang.reflect.Type;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class BaseEndpoint<E, M> {
    protected Response response;


    protected abstract Type getModel();

    public abstract E sendRequest();

    protected abstract int getSuccessStatusCode();

    public M getResponseModel() {
        assertThatResponseIsNotNull();

        return response.as(getModel());

    }


    public E assertRequestSuccess() {
        return assertStatusCode(getSuccessStatusCode());
    }

    public E assertStatusCode(int statusCode) {

        assertThatResponseIsNotNull();
        assertThat(response.getStatusCode()).as("Status code").isEqualTo(statusCode);
        return (E) this;
    }

    protected void assertThatResponseIsNotNull() {
        assertThat(response).as("Request response").isNotNull();
    }

}
