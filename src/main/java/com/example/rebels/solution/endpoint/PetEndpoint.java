package com.example.rebels.solution.endpoint;

import com.example.rebels.solution.model.pet.Pet;
import com.example.rebels.solution.service.PetService;
import com.example.rebels.solution.service.RestService;
import lombok.Setter;
import lombok.experimental.Accessors;

public abstract class PetEndpoint<E, M> extends BaseEndpoint<E, M> implements RestService<PetService> {
    @Setter
    @Accessors(fluent = true)
    protected int petId;
    @Setter
    @Accessors(fluent = true)
    protected Pet pet;

    @Override
    public PetService getServiceApi() {
        return new PetService();
    }
}
