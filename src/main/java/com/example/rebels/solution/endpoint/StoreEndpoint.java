package com.example.rebels.solution.endpoint;

import com.example.rebels.solution.service.RestService;
import com.example.rebels.solution.service.StoreService;
import lombok.Setter;
import lombok.experimental.Accessors;

public abstract class StoreEndpoint<E, M> extends BaseEndpoint<E, M> implements RestService<StoreService> {
    @Setter
    @Accessors(fluent = true)
    protected int orderId;

    @Override
    public StoreService getServiceApi() {
        return new StoreService();
    }
}
