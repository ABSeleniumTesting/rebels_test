# Rebels_Test

Solution for rebels test

## Project Tech Stack

This project was made by using:

* Maven
* Java 11 (OpenJDK)
* Serenity BDD
* Cucumber 6
* RestAssured


## System under the test

https://petstore.swagger.io/

## How to run this project

### Locally

__Requirements:__

* Java 11
* Maven
* Git
* Preferably some sort of IDEA (like Intellij)

__Installation/Configuration:__

1. Install Java 11 (preferably open jdk) on your machine.
Tutorial how to do it can be found [here](https://www.tutorialcup.com/java/install-java-11.htm)
   
2. Install and configure Maven (this step is required for running project via commandline).
Instructions how to do it can be found [here](https://maven.apache.org/install.html)
   
3. Install Git in order to clone/pull repository (or your fork).
Instructions how to do it can be found [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
   Also make sure that you install GitBash commandline during that process.

4. If you do not have IDEA, I suggest installing Intellij.
Instructions how to do it can be found [here](https://www.jetbrains.com/help/idea/installation-guide.html#standalone)
   
#### Running from commandline

__Steps:__

1. Open GitBash console
2. Go to the directory where the project is
3. Run command 
   `mvn -Dmaven.test.failurte.ignore=true clean install serenity:aggregate`


#### Running from IDEA

Depending on the IDEA you are using the steps might change. Steps that are listed here are for Intellij IDEA.

__Steps:__

1. Open the project in IDEA as a Maven project
2. Open Run/Debug Configurations
3. Create configuration for Maven
4. In the "goal" field put this command "mvn -Dmaven.test.failurte.ignore=true clean install serenity:aggregate"
5. Save/Apply configuration and close the window
6. Click on green play button (but make sure the proper configuration is chosen) to run the project



### By CICD via GitLab

__Requirements:__

* Have account on GitLab platform


__Steps:__

1. Fork the repository
2. Make your branch (Optional)
3. Make some changes in the code
4. Push those changes (it should trigger Pipeline in your fork)
5. Create pull request to master branch of the original project (Optional)


### Reporting

Since this project uses Serenity BDD framework, the test report is generated
automatically in form of HTML report.

#### Local
To open this report locally, you need to run the tests and ten go to this path:

"/target/site/serenity"

There you should find file _"index.html"_. Open it with the browser of your choice.


#### Via GitLab

After the pipeline is finished there should be two types of report.


One can be found in pipeline view, under the "Test" tab. It is standard Junit report.

Second should be published on GitLab Page. For example, like [this](https://abseleniumtesting.gitlab.io/rebels_test/)


## How to create new tests

__Requirements:__

* Prior knowledge of tools that this project is based of.
* Knowledge of "Request to Object" design pattern

### For new Endpoint

__Steps:__

1. Create new class that represents given Service on given Endpoint.
As an example of such implementation you can use "UserService" class.
   The class should contain methods that represent interactions available 
   on given Endpoint, also class should inherit from BaseService class to acquire base specification
   or to override it for its needs. The class should be under "service" package.
   
2. Create an Endpoint class that is an abstract class and extends from base endpoint.
It will later be used as a base for each new class that represent specific request.
   As an example please use "UserEndpoint" class. The class should be under "endpoint" package.
   
3. Under request package create a new package for the new endpoint requests.
4. In that new package create for each interaction/request class. Each class
should be extended by the new endpoint class to implement generic methods. Also class name
   should suggest the type of request and interaction name, for example "GetFindUserRequest".
   
5. Create a new directory for new feature files.
   
6. Create feature files to describe test scenarios for the new endpoint.
You can use previously made feature files as an example.
   
7. Create new "Step" class that is bound to specific endpoint and will
only have specific steps for that endpoint interactions/tests.
   
8. If there is any common step that would be used for more than one feature file, please
put it in the "CommonSteps" class. Furthermore, if it creates some kind of variable/object or state,
    then you can use Serenity session variables to share this with the rest of test steps outside "CommonSteps" class.
   
9. Use "dry-run" flag for step generation (optional)
10. Test your new scenarios locally
11. Push the changes to repository

### For new resource/interaction/test case on existing endpoint

#### Resource/Interaction

__Steps:__

1. Create a new method in appropriate class that represents already existing service/endpoint.
2. Create a new class that represents new interaction/request under proper
package with given service name. Make sure you extend proper endpoint class.
   
3.Create new feature file (or use existing ones if for example new interaction
is to find a User by his name)
4. Generate steps and implement them to proper steps class (generic steps should go to "CommonSteps" class)
5. Test your new scenarios locally
6. Push the changes


#### New test cases for existing scenario

__Steps:__

1.Create new feature file (or use existing ones if for example new interaction
is to find a User by his name)
2. Generate steps and implement them to proper steps class (generic steps should go to "CommonSteps" class)
3. Test your new scenarios locally
4. Push the changes



To make code more readable and to hide complex logic, you can create your own helper classes,
just like "PetHelper" class. If you need some files or test data to generate/use
you can use the "StepHelper" class which mostly holds methods to extract test data from
test files in test resources. This class also contains constraints/labels for session variables.

__IF__ you want, you can create pull request to this repository with new tests (PR should point to master branch of this repository). After PR passes the
pipeline check, then the merge would be available (however it is done manually).